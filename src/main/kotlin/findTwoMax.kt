import com.google.common.collect.Ordering

fun findTwoMax(input: Collection<Int>): Pair<Int, Int> {
    assert(input.size >= 2)

    var first = Int.MIN_VALUE
    var second = Int.MIN_VALUE

    input.map {
        if (it > first) {
            second = first
            first = it
        } else if (it > second) {
            second = it
        }
    }

    return Pair(first, second)
}

fun findTwoMaxBySort(input: Collection<Int>): Pair<Int, Int> {
    assert(input.size >= 2)

    val result = input
        .sorted()
        .takeLast(2)

    return Pair(result[1], result[0])
}

fun findNMaxInGuava(input: Collection<Int>, top: Int = 2): List<Int> {
    assert(input.size >= top)

    return Ordering.natural<Comparable<Int>>().greatestOf(input, top)
}
