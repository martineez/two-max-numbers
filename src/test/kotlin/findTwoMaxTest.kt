
import org.junit.jupiter.api.Test
import kotlin.system.measureTimeMillis
import kotlin.test.assertEquals

@ExperimentalStdlibApi
class TwoMaxNumbersTest {

    private val numbers = (0..150_000).shuffled().toList()

    @Test
    fun `findTwoMax`() {
        val result: Pair<Int, Int>

        println("findTwoMax took ${measureTimeMillis { result = findTwoMax(numbers) }}ms")

        assertEquals(Pair(150_000, 149_999), result)
    }

    @Test
    fun `findTwoMaxBySort`() {
        val result: Pair<Int, Int>

        println("findTwoMaxBySort took ${measureTimeMillis { result = findTwoMaxBySort(numbers) }}ms")

        assertEquals(Pair(150_000, 149_999), result)
    }

    @Test
    fun `findNMaxInGuava`() {
        val result: List<Int>
        println("findNMaxInGuava took ${measureTimeMillis { result = findNMaxInGuava(numbers, 10) }}ms")

        assertEquals((150_000 downTo 149_991 ).toList(), result)
    }
}
